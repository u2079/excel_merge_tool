﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace ExcelMerge
{
    public partial class Form1 : Form
    {

        string filePathSource = string.Empty;

        string filePathResult = string.Empty;
        string filePathResultDirectory = string.Empty;


        Excel.Application _excelApp;
        Excel.Workbook _workBookSource;
        Excel.Workbook _workBookResult;

        string lockFile = "lock.lock";
        bool locked = false;

        const string sheetName = "Liste offener Fehler";
        string summary = "";

        const string TICKET_NR = "ticket no.";
        const string PL_COMMENT = "PL comment";
        const string PL_EXTRA = "PL status";
        const string FM_COMMENT = "FM comment";
        const string WIEDERVORLAGE = "Wiedervorlage";

        const int HEADER_ROW = 2;

        Dictionary<string, int> columnsSource = new Dictionary<string, int>() {
            { TICKET_NR, -1},
            { PL_COMMENT, -1},
            { PL_EXTRA, -1},
            { FM_COMMENT, -1},
            { WIEDERVORLAGE, -1}
        };

        // must be the same for the result!! can be changes in the row id, but the columns still the same

        List<Dictionary<string, string>> listOfUpdates = new List<Dictionary<string, string>>();
        /*Dictionary<string, string> rowResult = new Dictionary<string, string>() {
            { TICKET_NR, ""},
            { PL_COMMENT, ""},
            { PL_EXTRA, ""},
            { FM_COMMENT, ""},
            { WIEDERVORLAGE, -1}
        };*/


        internal static class CellColors
        {
            public static Color Wrong = Color.FromArgb(255, 255, 0, 0);
            public static Color Ok = Color.FromArgb(255, 146, 208, 80);
            public static Color Warning = Color.FromArgb(255, 255, 255, 0);
            public static Color NotFound = Color.FromArgb(255, 255, 255, 0);
            public static Color Default = Color.White;
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "xls files (*.xls)|*.xls|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    filePathSource = openFileDialog.FileName;
                    label1.Text = filePathSource;      
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.InitialDirectory = "c:\\";
                saveFileDialog.Filter = "xls files (*.xls)|*.xls|All files (*.*)|*.*";
                saveFileDialog.FilterIndex = 2;
                saveFileDialog.RestoreDirectory = true;

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePathResult = saveFileDialog.FileName;
                    label3.Text = filePathResult;

                    FileInfo info = new FileInfo(filePathResult);
                    var split = filePathResult.Split('/');
                    filePathResultDirectory = info.Directory.FullName;

                    lockFile = info.Name + ".lock";

                    checkLock();
                }
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            checkLock();

            if (locked) {
                MessageBox.Show($"The file {filePathResult} is locked, wait to try again the action");
                return;
            }

            createLock();

            checkLock();

            if (File.Exists(filePathSource) && File.Exists(filePathResult))
            {
                
                // not needed 
                //CreateWorkingCopy();

                initExcelAppAndBook();

                InitColumnPositions(_workBookSource);
                ExtractUpdates();

                // for differents type of source-results files
                columnsSource = new Dictionary<string, int>() {
                    { TICKET_NR, -1},
                    { PL_COMMENT, -1},
                    { PL_EXTRA, -1},
                    { FM_COMMENT, -1},
                    { WIEDERVORLAGE, -1}
                };
                InitColumnPositions(_workBookResult);
                FillUpdates();

                closeExcelAppAndBooks();

                MessageBox.Show($"Finished: {summary}");
            }
            else
            {
                richTextBox.Text = "One or more files are invalid, please select the right one";
            }

            removeLock();

            checkLock();
        }

        public void initExcelAppAndBook()
        {
            try
            {
                _excelApp = (Excel.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application");
            }
            catch
            {
                _excelApp = new Excel.Application();
            }

            if (_excelApp == null)
                _excelApp = new Excel.Application();

            try
            {
                _workBookSource = _excelApp.Workbooks[System.IO.Path.GetFileName(filePathSource)];
            }
            catch
            {              
                _workBookSource = _excelApp.Workbooks.Open(filePathSource);
            }

            try
            {
                _workBookResult = _excelApp.Workbooks[System.IO.Path.GetFileName(filePathResult)];
            }
            catch
            {
                _workBookResult = _excelApp.Workbooks.Open(filePathResult);
            }

            _excelApp.Visible = false;
        }

        public void InitColumnPositions(Excel.Workbook workBook)
        {
            richTextBox.Text += "\nExtracting columns positions...";
            Excel.Worksheet worksheet = GetSheet(workBook, sheetName);

            // worksheet not found
            if (worksheet == null)
            {
                return;
            }

            int maxColumns = worksheet.UsedRange.Columns.Count;

           
            for (int i = 1; i <= maxColumns; i++)
            {
                try
                {
                    Excel.Range currentRange = (Excel.Range)worksheet.Cells[HEADER_ROW, i];
                    if (currentRange.Value2 != null)
                    {
                        string value = currentRange.Value2.ToString();

                        // there are two ticket no. !! We are selecting olnly the first
                        if (value.Equals(TICKET_NR) && columnsSource[TICKET_NR] == -1)
                        {
                            columnsSource[TICKET_NR] = i;
                        }
                        if (value.Equals(PL_COMMENT))
                        {
                            columnsSource[PL_COMMENT] = i;
                        }
                        if (value.Equals(PL_EXTRA))
                        {
                            columnsSource[PL_EXTRA] = i;
                        }
                        if (value.Equals(FM_COMMENT))
                        {
                            columnsSource[FM_COMMENT] = i;
                        }
                        if (value.Equals(WIEDERVORLAGE))
                        {
                            columnsSource[WIEDERVORLAGE] = i;
                        }
                    }
                }
                catch { }
            }


            summary += $"\nPositions for the colummns: \n\t{TICKET_NR}: {columnsSource[TICKET_NR]} \t{PL_COMMENT}: {columnsSource[PL_COMMENT]} \t{PL_EXTRA}: {columnsSource[PL_EXTRA]} \t{FM_COMMENT}: {columnsSource[FM_COMMENT]} \t{WIEDERVORLAGE}: {columnsSource[WIEDERVORLAGE]}";
            richTextBox.Text = summary;
        }

        public void ExtractUpdates()
        {           
            // from source fill the updated rows
            Excel.Worksheet worksheet = GetSheet(_workBookSource, sheetName);

            if (worksheet == null)
            {
                summary += $"\nNot found sheet with the indicated named: {sheetName} ";
                richTextBox.Text = summary;
                return;
            }
                
            int maxRows = worksheet.UsedRange.Rows.Count;

            listOfUpdates.Clear();

            richTextBox.Text += $"\nExtracting data to be updated (from {maxRows} rows)";
            string updatesSummary = "";

            for (int i = HEADER_ROW + 1; i <= maxRows; i++)
            {
                try
                {
                    // as progress indicator... each 10 rows we show a .
                    if (i%10 == 0)  
                        richTextBox.Text += ".";

                    var ticket = "";
                    try
                    {
                        ticket = (string)(worksheet.Cells[i, columnsSource[TICKET_NR]] as Excel.Range).Value2.ToString().Trim();
                    } catch{
                        // if we don´t have ticket id, we can ignore it!
                        continue;
                    }

                    var pl_comment = "";
                    try
                    {
                        pl_comment = (string)(worksheet.Cells[i, columnsSource[PL_COMMENT]] as Excel.Range).Value2.ToString().Trim();
                    }
                    catch {}

                    var pl_extra = "";
                    try
                    {
                        pl_extra = (string)(worksheet.Cells[i, columnsSource[PL_EXTRA]] as Excel.Range).Value.ToString().Trim();
                    }
                    catch {}

                    var fm_comment = "";
                    try
                    {
                        fm_comment = (string)(worksheet.Cells[i, columnsSource[FM_COMMENT]] as Excel.Range).Value.ToString().Trim();
                    }
                    catch { }

                    var wiedervorlage = "";
                    try
                    {
                        wiedervorlage = (string)(worksheet.Cells[i, columnsSource[WIEDERVORLAGE]] as Excel.Range).Value.ToString().Trim();
                    }
                    catch { }

                    if (pl_comment != "" || pl_extra != "" || fm_comment != "" || wiedervorlage != "")
                    {
                        Dictionary<string, string> rowResult = new Dictionary<string, string>() {
                            { TICKET_NR, ticket},
                            { PL_COMMENT, pl_comment},
                            { PL_EXTRA, pl_extra},
                            { FM_COMMENT, fm_comment},
                            { WIEDERVORLAGE, wiedervorlage}
                        };

                        string rowSummary = $"\n\t{TICKET_NR}: [{ticket}] \t{PL_COMMENT}: [{pl_comment}] \t{PL_EXTRA}: [{pl_extra}] \t{FM_COMMENT}: [{fm_comment}] \t{WIEDERVORLAGE}: [{wiedervorlage}]";
                        richTextBox.Text += rowSummary;
                        updatesSummary += rowSummary;

                        listOfUpdates.Add(rowResult);
                    }
                        
                }
                catch (Exception e) {
                    Console.WriteLine("exception:" + e.Message);
                }           
            }

            summary += "\nUpdates found:" + updatesSummary;

            richTextBox.Text = summary;
        }

        public void FillUpdates() {
            // update the info in the result excel            
            Excel.Worksheet worksheet = GetSheet(_workBookResult, sheetName);

            if (worksheet == null)
            {
                summary += $"\nNot found sheet with the indicated named in the target file: : {sheetName} ";
                richTextBox.Text = summary;
                return;
            }

            int maxRows = worksheet.UsedRange.Rows.Count;

            string updateSummary = "";

            richTextBox.Text += $"\nSearching tickets in the target file (from {maxRows} rows)";

            // it´s faster if we have the list of tickets to be updated as identifiers list
            var tickets = new List<string>();

            foreach (var t in listOfUpdates)
            {
                tickets.Add(t[TICKET_NR]);
            }

            for (int i = HEADER_ROW + 1; i <= maxRows; i++)
            {
                try
                {
                    // as progress indicator... each 10 rows we show a .
                    if (i % 10 == 0)
                        richTextBox.Text += ".";

                    var ticket = "";
                    try
                    {
                        ticket = (string)(worksheet.Cells[i, columnsSource[TICKET_NR]] as Excel.Range).Value2.ToString().Trim();

                        // if ticket in tickets ==>> we have to update the entry, if not we can ignore it
                        if (!tickets.Contains(ticket))
                            continue;

                        // if we have data for this ticket, we extract it to update later
                    }
                    catch
                    {
                        // if we don´t have ticket id, we can ignore it!
                        continue;
                    }

                    var pl_comment = "";
                    try
                    {
                        pl_comment = (string)(worksheet.Cells[i, columnsSource[PL_COMMENT]] as Excel.Range).Value2.ToString().Trim();
                    }
                    catch { }

                    var pl_extra = "";
                    try
                    {
                        pl_extra = (string)(worksheet.Cells[i, columnsSource[PL_EXTRA]] as Excel.Range).Value.ToString().Trim();
                    }
                    catch { }

                    var fm_comment = "";
                    try
                    {
                        fm_comment = (string)(worksheet.Cells[i, columnsSource[FM_COMMENT]] as Excel.Range).Value.ToString().Trim();
                    }
                    catch { }

                    var wiedervorlage = "";
                    try
                    {
                        wiedervorlage = (string)(worksheet.Cells[i, columnsSource[WIEDERVORLAGE]] as Excel.Range).Value.ToString().Trim();
                    }
                    catch { }

                    /*          Dictionary<string, string> rowResult = new Dictionary<string, string>() {
                    { TICKET_NR, ""},
                    { PL_COMMENT, ""},
                    { PL_EXTRA, ""},
                    { FM_COMMENT, ""},
                    { WIEDERVORLAGE, ""}
                };*/
                    Dictionary<string, string> rowUpdate = null;
                    foreach (var t in listOfUpdates)
                    {
                        if (t[TICKET_NR] == ticket)
                        {
                            rowUpdate = t;
                            break;
                        }
                    }

                    string rowSummary = $"\nTicket:{ ticket}";

                    // we have to update the columns
                    if (columnsSource[PL_COMMENT] != -1)
                    {
                        Excel.Range cellComment = worksheet.Cells[i, columnsSource[PL_COMMENT]];
                        // if the cell contains the comment, we are not adding it
                        if (rowUpdate[PL_COMMENT] == "")
                        {
                            // do nothing
                        }
                        else if (pl_comment.Contains(rowUpdate[PL_COMMENT]))
                        {
                            rowSummary += $"\n\t{PL_COMMENT}: This comment allready exists for this ticket: {rowUpdate[PL_COMMENT]}";
                        }
                        else
                        {
                            // comment at the end
                            // string text = (pl_comment != "" ? pl_comment + ", " : "") + rowUpdate[PL_COMMENT];
                            // comment at the begin
                            string text = rowUpdate[PL_COMMENT] + (pl_comment == "" ? "" : ",\n" + pl_comment);
                            cellComment.Value = text;
                            rowSummary += $"\t{PL_COMMENT}: {text} ";
                        }
                    }

                    if (columnsSource[PL_EXTRA] != -1)
                    {
                        Excel.Range cellStatus = worksheet.Cells[i, columnsSource[PL_EXTRA]];
                        //cellStatus.Value = (pl_extra != "" ? pl_extra + ", " : "") + rowUpdate[PL_EXTRA];
                        // if the cell contains the comment, we are not adding it
                        if (rowUpdate[PL_EXTRA] == "")
                        {
                            // do nothing
                        }
                        else if (pl_extra.Contains(rowUpdate[PL_EXTRA]))
                        {
                            rowSummary += $"\n\t{PL_EXTRA}: This comment allready exists for this ticket: {rowUpdate[PL_EXTRA]}";
                        }
                        else
                        {
                            // string text = (pl_extra != "" ? pl_extra + ", " : "") + rowUpdate[PL_EXTRA];
                            string text = rowUpdate[PL_EXTRA] + (pl_extra == "" ? "" : ",\n" + pl_extra);
                            cellStatus.Value = text;
                            rowSummary += $"\t{PL_EXTRA}: {text} ";
                        }
                    }

                    if (columnsSource[FM_COMMENT] != -1)
                    {
                        Excel.Range cellFmComment = worksheet.Cells[i, columnsSource[FM_COMMENT]];
                        //cellFmComment.Value = (fm_comment != "" ? fm_comment + ", " : "") + rowUpdate[FM_COMMENT];
                        // if the cell contains the comment, we are not adding it
                        if (rowUpdate[FM_COMMENT] == "")
                        {
                            // do nothing
                        }
                        else if (fm_comment.Contains(rowUpdate[FM_COMMENT]))
                        {
                            rowSummary += $"\n\t{FM_COMMENT}: This comment allready exists for this ticket: {rowUpdate[FM_COMMENT]}";
                        }
                        else
                        {
                            // string text = (fm_comment != "" ? fm_comment + ", " : "") + rowUpdate[FM_COMMENT];
                            string text = rowUpdate[FM_COMMENT] + (fm_comment == "" ? "" : ",\n" + fm_comment);
                            cellFmComment.Value = text;
                            rowSummary += $"\t{FM_COMMENT}: {text} ";
                        }
                    }

                    if (columnsSource[WIEDERVORLAGE] != -1)
                    {
                        Excel.Range cellWiedervorlage = worksheet.Cells[i, columnsSource[WIEDERVORLAGE]];
                        // if the cell contains the comment, we are not adding it
                        if (rowUpdate[WIEDERVORLAGE] == "")
                        {
                            // do nothing
                        }
                        else if (wiedervorlage.Contains(rowUpdate[WIEDERVORLAGE]))
                        {
                            rowSummary += $"\n\t{WIEDERVORLAGE}: This comment allready exists for this ticket: {rowUpdate[WIEDERVORLAGE]}";
                        }
                        else
                        {
                            // string text = (fm_comment != "" ? fm_comment + ", " : "") + rowUpdate[FM_COMMENT];
                            string text = rowUpdate[WIEDERVORLAGE] + (wiedervorlage == "" ? "" : ",\n" + wiedervorlage);
                            cellWiedervorlage.Value = text;
                            rowSummary += $"\t{WIEDERVORLAGE}: {text} ";
                        }
                    }

                    // we can show the current progress without waiting for the entire process
                    richTextBox.Text += rowSummary;
                    richTextBox.SelectionStart = richTextBox.Text.Length;
                    richTextBox.ScrollToCaret();

                    updateSummary += rowSummary;
                }
                catch (Exception e)
                {
                    Console.WriteLine("exception:" + e.Message);
                }               
            }

            summary += "\nIn target file:" + updateSummary;
            richTextBox.Text = summary;
        }
               
        public void closeExcelAppAndBooks()
        {
            try
            {
                _excelApp.DisplayAlerts = false; // we don´t want to show the overwrite dialog

                if (_workBookSource != null)
                {
                    // _workBook1.SaveAs(filePath1);
                    _workBookSource.Close(0);
                }

                if (_workBookResult != null)
                {
                    _workBookResult.SaveAs(filePathResult);
                    _workBookResult.Close(0);
                }
                if (_excelApp != null)
                {
                    _excelApp.Quit();
                }
            }
            catch (Exception e)
            {
                summary += $"\nException saving file: {filePathResult}\nException: {e.Message} ";
                richTextBox.Text = summary;
            }
        }

        public void CreateWorkingCopy()
        {            
            FileInfo fi = new FileInfo(filePathResult);
            FileInfo fiOriginal = new FileInfo(filePathResult);
            if (!fi.Exists && fiOriginal.Exists)
            {
                fiOriginal.CopyTo(filePathResult);
            }
        }

        public Excel.Worksheet GetSheet(Excel.Workbook workBook, string name)
        {
            if (workBook == null)
                return null;

            try
            {                
                Excel.Worksheet sheet = workBook.Sheets[name];
                return sheet;
            }
            catch
            {
                // return the first? 
                foreach (Excel.Worksheet sheet in workBook.Sheets) {
                    summary += $"\nNot found sheet [{name}]: using [{sheet.Name}] ";
                    return sheet;
                }
                return null;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            checkLock();
        }

        void checkLock()
        {
            // check if there is a lock file in the directory 
            locked = false;
            string path = filePathResultDirectory + "/" + lockFile;

            if (File.Exists(path))
                locked = true;

            pictureBox1.Image = locked ? Properties.Resources.locked : Properties.Resources.unlocked;
        }
        void createLock()
        {
            string path = filePathResultDirectory + "/" + lockFile;

            try {
                if (!File.Exists(path))
                {
                    var f = File.Create(path);
                    f.Close();
                }
            }
            catch { }
        }
        void removeLock()
        {
            string path = filePathResultDirectory + "/" + lockFile;
            try
            {
                if (File.Exists(path))
                    File.Delete(path);
            }
            catch { }
        }

        private void richTextBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
